const events = require('events')
const SocketPacket = require('socket-packet')

const BroadcastSocket = function (options, rpcs, context) {
  const dgram = require('dgram')

  const _this = this
  const _server = dgram.createSocket('udp4')
  const _rpcs = rpcs || {}
  const _address = options.address
  const _broadcastAddress = options.broadcastAddress
  const _hostPort = options.port

  SocketPacket.bind(_server, null, {
    packetStringifier: packet => packet && JSON.stringify(packet),
    packetParser: packet => packet && JSON.parse(packet),
    type: 'dgram'
  })

  _server.__dispatch = _server.dispatch

  _server.dispatch = (__id, data, rInfo, cb) => {
    data = data || {}
    data.__id = __id

    const port = (rInfo && rInfo.port) || _hostPort
    const address = (rInfo && rInfo.address) || _broadcastAddress

    _server.__dispatch(data, port, address, cb || (() => {}))
  }

  _server.on('packet', (data, rInfo) => {
    if (_server.address === _address && _server.port === _this.address().port) {
      return
    }

    if (!data) {
      return
    }

    const __id = data.__id

    delete data.__id
    data.__socket = _server

    if (__id in _rpcs) {
      const resp = new function (rInfo) {
        const _this = this

        _this.send = (id, data, cb) => {
          _server.dispatch(id, { data: data }, rInfo, cb)
        }

        _this.broadcast = (id, data, cb) => {
          _server.dispatch(id, { data: data }, null, cb)
        }

        return _this
      }(rInfo)

      if (context) {
        return _rpcs[__id].call(context, data, resp)
      }
      return _rpcs[__id](data, resp)
    }

    _this.emit('unknownMessage', __id, data, rInfo)
  })

  _server.on('listening', () => {
    _server.setBroadcast(true)
    _this.emit('listening', _this.address())
  })
  _server.on('error', () => {
    _this.emit.callApply(_this, 'error', arguments)
  })
  _server.on('close', () => {
    _this.emit.callApply(_this, 'close', arguments)
  })

  _this.address = () => _server.address()
  _this.send = _server.dispatch
  _this.close = () => _server.close()
  _this.start = cb => _server.bind(_hostPort, null, cb || (() => {}))

  return _this
}

BroadcastSocket.prototype = events.EventEmitter.prototype
module.exports = BroadcastSocket
