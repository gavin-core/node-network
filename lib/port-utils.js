const core = require('node-core')
const net = require('net')

const numbers = core.numbers

module.exports = {
  getPort: function () {
    let min
    let max
    let cb

    switch (arguments.length) {
      case 2:
        min = arguments[0].min
        max = arguments[0].max
        cb = arguments[1]
        break
      case 3:
        min = arguments[0]
        max = arguments[1]
        cb = arguments[2]
        break
      default:
        throw new Error('ERROR')
    };

    const server = net.createServer()

    const port = numbers.getRandomInt(min, max)
    server.listen(port, 'localhost', () => {
      server.once('close', () => {
        cb(null, port)
      })

      server.close()
    })
    server.on('error', () => {
      module.exports.getPort(min, max, cb)
    })
  }
}
