const events = require('events')
const SocektPacket = require('socket-packet')

const SocketServer = function (options, hpcs, rpcs, context) {
  const net = require('net')
  const core = require('node-core')
  const RpcManager = require('./rpc-manager')

  const logger = core.logger
  const _ = core._

  const _this = this
  let _server
  const _context = context

  const _handleClientDisconnect = socket => {
    _this.sockets.splice(_this.sockets.indexOf(socket), 1)
    socket.emit('disconnect', socket)
  }

  const _initSocket = s => {
    SocektPacket.bind(s, null, {
      packetStringifier: packet => packet && JSON.stringify(packet),
      packetParser: packet => packet && JSON.parse(packet)
    })
    s.setTimeout(0)
    s.setNoDelay(true)
    _this.sockets.push(s)

    RpcManager.call(s, 'SERVER_SOCKET_', hpcs, rpcs)

    s.sendJson = function (id, data, cb, rid) {
      s.dispatch({ __id: id, __rid: rid, data: data }, cb || (() => {}))
    }

    s.sendError = function (id, err, cb, rid) {
      s.dispatch({ __id: id, __rid: rid, err: err }, cb || (() => {}))
    }

    s.on('packet', data => {
      if (data.__rid in s.rpcCBs) {
        const fn = s.rpcCBs[data.__rid]
        delete s.rpcCBs[data.__rid]
        return fn(data.err, data.data)
      }

      const id = data.__id
      const idCamelCased = id.toCamelCase()

      const resp = new function initResponse (socket, id, data) {
        this.send = (d, cb) => {
          socket.sendJson(id, d, cb, data.__rid)
        }

        this.sendError = (err, cb) => {
          socket.sendError(id, err, cb, data.__rid)
        }
      }(s, id, data)

      data.__socket = s

      if (idCamelCased in s.hpcs) {
        if (_context) {
          return s.hpcs[idCamelCased].call(_context, data, resp)
        }
        return s.hpcs[idCamelCased](data, resp)
      }

      _this.emit('unknown-message', id, data, resp)
      logger.error('No packet handler found for id:' + id)
    })

    s.on('error', _this.emit)
    s.on('end', _this.emit) // TODO - is this needed ?
    s.on('close', () => {
      _handleClientDisconnect(s)
    })

    return s
  }

  _this.sockets = []

  _server = net.createServer()

  _server.on('connection', function () {
    arguments[0] = _initSocket(arguments[0])
    _this.emit.callApply(_this, 'connection', arguments)
  })
  _server.on('listening', function () {
    _this.emit.callApply(_this, 'listening', arguments)
  })
  _server.on('error', function (err) {
    logger.error('SOCKET SERVER ERROR: ' + err.stack)
    _this.emit.callApply(_this, 'error', arguments)
  })
  _server.on('close', function () {
    _this.emit.callApply(_this, 'close', arguments)
  })

  _this.broadcast = function (id, data, whereOptions, cb, timeoutDelay) {
    whereOptions = whereOptions || {}
    cb = cb || (() => {})

    let sockets
    let sentCount = 0
    let timeout

    const doneCb = function () {
      clearTimeout(timeout)
      cb.callApply(cb, arguments)
    }

    const onSentCb = () => {
      sentCount++
      if (sentCount >= sockets.length) {
        doneCb()
      }
    }

    if (typeof whereOptions === 'function') {
      sockets = _.filter(_this.sockets, whereOptions)
    } else {
      sockets = _.where(_this.sockets, whereOptions)
    }

    if (!sockets || !sockets.length) {
      return doneCb()
    }

    for (let i = 0; i < sockets.length; i++) {
      sockets[i].sendJson(id, data, onSentCb)
    }

    timeout = setTimeout(doneCb, timeoutDelay || 5000)
  }

  _this.address = () => _server.address()

  _this.close = cb => {
    _server.close(cb)
  }

  _this.destroy = cb => {
    _this.close(cb)
    for (let i = _this.sockets.length - 1; i >= 0; i--) {
      _this.sockets[i].end()
    }
  }

  _this.listen = cb => {
    _server.listen({
      port: options.port,
      host: options.host
    }, cb || (() => {}))
  }

  return _this
}

SocketServer.prototype = events.EventEmitter.prototype
module.exports = SocketServer
