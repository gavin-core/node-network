const core = require('node-core')
const _ = core._

const RpcManager = function (prefix, hpcs, rpcs) {
  const _this = this
  const _prefix = prefix
  let _callIdSeed = 1

  const _getId = () => {
    if (_callIdSeed > 100000000000) {
      _callIdSeed = 0
    }
    return '' + _prefix + _callIdSeed++
  }

  const _generateRPCs = rpcs => {
    rpcs = rpcs || []
    const result = {}

    for (let i = 0; i < rpcs.length; i++) {
      result[rpcs[i].toCamelCase('_')] = new function (rpc) {
        return (data, cb) => {
          if (typeof data === 'function') {
            cb = data
            data = {}
          }

          const rid = _getId()
          _this.rpcCBs[rid] = cb || (() => {})

          _this.sendJson(rpc, data, null, rid)
        }
      }(rpcs[i])
    }

    return result
  }

  _this.hpcs = {}
  _this.rpcs = {}
  _this.rpcCBs = {}

  _this.registerHPCs = hpcs => {
    _this.hpcs = _.extend(_this.hpcs, hpcs)
  }

  _this.setHPCs = hpcs => {
    _this.hpcs = _.extend({}, hpcs || {})
  }

  _this.registerRPCs = rpcs => {
    _this.rpcs = _.extend(_this.rpcs, _generateRPCs(rpcs))
  }

  _this.setRPCs = rpcs => {
    _this.rpcs = _.extend({}, _generateRPCs(rpcs))
  }

  _this.setHPCs(hpcs)
  _this.setRPCs(rpcs)

  return _this
}

module.exports = RpcManager
