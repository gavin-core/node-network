const dns = require('dns')
const urijs = require('uri-js')

/*******************************************
usage:
new URI(protocol, host, port)
*******************************************/
const URI = function () {
  const _this = this

  switch (arguments.length) {
    case 2:
      _this.protocol = arguments[0]

      if (typeof arguments[1] === 'string') {
        const parts = arguments[1].split(',')
        _this.host = parts.splice(0, 4).join('.')
        _this.port = (+parts[0]) * 256 + +parts[1]
      } else {
        _this.host = arguments[1].address
        _this.port = arguments[1].port
      }
      break
    case 3:
      _this.protocol = arguments[0]
      _this.host = arguments[1]
      _this.port = arguments[2]
      break
    default:
      throw new Error('ERROR')
  }

  if (_this.host.split('.').length === 4 && (+_this.host.split('.')[0] || _this.host.split('.')[0] === '0')) {
    // host is an ip address
    _this.address = _this.host

    dns.reverse(_this.host, (err, domains) => {
      if (err) {
        return
      }

      _this.hostname = domains[0]
    })
  } else {
    // host is a hostname
    _this.hostname = _this.host

    dns.lookup(_this.host, 4, (err, address) => {
      if (err) {
        console.log('Error received but not catered for')
        console.error(err)
      }
      _this.address = address
    })
  }

  _this.toString = () => `${_this.protocol}://${_this.host}:${_this.port}`

  _this.to256 = () => {
    if (!_this.address) {
      return 'unknown'
    }

    let result = ''
    const address = _this.address.split('.').join(',')
    const portParts = [
      Math.floor(_this.port / 256),
      _this.port % 256
    ]

    result += address
    for (var i = 0; i < portParts.length; i++) {
      result += ',' + portParts[i]
    }

    return result
  }

  return _this
}

module.exports = {
  URI: URI,
  parse: value => {
    const uri = urijs.parse(value)

    uri.userinfo = uri.userinfo.replace(/%40/g, '@')
    uri.username = null
    uri.password = null

    if (uri.userinfo && uri.userinfo) {
      const parts = uri.userinfo.split(':')
      uri.username = parts.shift()
      if (parts.length) {
        uri.password = parts.shift()
      }
    }

    return uri
  },
  serialize: urijs.serialize
}
