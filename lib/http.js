const qs = require('querystring')

module.exports = type => {
  const protocol = require(type)

  const _this = {
    request: (params, body, cb) => {
      if (typeof body === 'function') {
        cb = body
        body = null
      }

      cb = cb || (() => {})

      const resCb = response => {
        let str = ''

        response.on('data', chunk => {
          str += chunk.toString('utf8')
        })

        response.on('end', () => {
          let data
          try {
            data = JSON.parse(str)
          } catch (e) {}

          if (response.statusCode === 200) {
            return cb(null, data || str)
          }

          if (response.statusCode === 404) {
            return cb()
          }

          cb(data || str)
        })

        response.on('error', err => {
          console.log('HTTP caught error:')
          console.error(err)
          return cb(err)
        })
      }

      try {
        if (params.query) {
          const queryString = qs.stringify(params.query)
          params.path += '?' + queryString
          delete params.query
        }

        if (body) {
          body = JSON.stringify(body)
          params.headers = params.headers || {}

          if (!params.headers['Content-Type']) {
            params.headers['Content-Type'] = 'application/json'
          }
        }

        if (params.path) {
          // replace splaces in url
          params.path = params.path.replace(/\s/g, '%20')
        }

        const req = protocol.request(params, resCb)
        if (body) {
          req.write(body)
        }

        let timedOut = false
        req.on('error', function () {
          if (!timedOut) {
            cb.apply(cb, arguments)
          }
        })

        // TODO put in config somewhere
        const timeout = params.timeout || 3000

        req.setTimeout(timeout, () => {
          timedOut = true
          req.abort()
          cb(new Error('HTTP timeout calling'))
        })

        req.end()
      } catch (err) {
        return cb(err)
      }
    }
  }

  return _this
}
