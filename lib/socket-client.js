const events = require('events')
const SocketPacket = require('socket-packet')

const SocketClient = function (options, hpcs, rpcs, context) {
  const net = require('net')
  const core = require('node-core')
  const RpcManager = require('./rpc-manager')

  const logger = core.logger

  const _this = this
  let _client = null
  const _context = context

  RpcManager.call(_this, 'CLIENT_', hpcs, rpcs)

  _client = net.connect(options, () => {
    SocketPacket.bind(_client, null, {
      packetStringifier: packet => packet && JSON.stringify(packet),
      packetParser: packet => packet && JSON.parse(packet)
    })
    _this.remoteAddress = _client.remoteAddress
    _this.emit('connect')
  })
  _client.setTimeout(0)
  _client.setNoDelay(true)

  _client.on('packet', data => {
    if (data.__rid in _this.rpcCBs) {
      const fn = _this.rpcCBs[data.__rid]
      delete _this.rpcCBs[data.__rid]
      return fn(data.err, data.data)
    }

    const id = data.__id
    const idCamelCased = id.toCamelCase()
    const resp = new function initResponse (id, data) {
      this.send = (d, cb) => {
        _this.sendJson(id, d, cb, data.__rid)
      }

      this.sendError = (err, cb) => {
        _this.sendError(id, err, cb, data.__rid)
      }
    }(id, data)

    if (idCamelCased in _this.hpcs) {
      if (_context) {
        return _this.hpcs[idCamelCased].call(_context, data, resp)
      }
      return _this.hpcs[idCamelCased](data, resp)
    }

    // console.log('unknown-message: ' + id);
    // _this.emit('unknown-message', id, data, resp);
    // logger.error('No packet handler found for id:' + id);

    _this.emit(data.__id, data)
  })

  _client.on('error', _this.emit)

  _client.on('end', _this.emit)

  _client.on('close', () => {
    _client = null
    logger.debug('Client disconnected')
    _this.emit('disconnect')

    for (let rid in _this.rpcCBs) {
      if (_this.rpcCBs.hasOwnProperty(rid)) {
        logger.warning('packet not processed before disconnect from server')
        _this.rpcCBs[rid]('disconnected from server')
        delete _this.rpcCBs[rid]
      }
    }
  })

  _this.sendJson = function (id, data, cb, rid) {
    cb = cb || (() => {})

    if (!_client) {
      if (_this.rpcCBs[rid]) {
        cb = _this.rpcCBs[rid]
        delete _this.rpcCBs[rid]
      }
      return cb(new Error('TCP Socket closed, cannot send message'))
    }

    _client.dispatch({ __id: id, __rid: rid, data: data }, cb)
  }

  _this.sendError = function (id, err, cb, rid) {
    cb = cb || (() => {})

    if (!_client) {
      if (_this.rpcCBs[rid]) {
        cb = _this.rpcCBs[rid]
        delete _this.rpcCBs[rid]
      }
      return cb(new Error('TCP Socket closed, cannot send message'))
    }

    _client.dispatch({ __id: id, __rid: rid, err: err }, cb)
  }

  _this.address = () => _client.address()

  _this.setTimeout = (timeout, cb) => {
    if (_client) {
      _client.setTimeout(timeout, cb)
    }
  }

  _this.close = _this.end = () => {
    if (_client) {
      return _client.end()
    }
  }
  _this.destroy = () => {
    if (_client) {
      return _client.destroy()
    }
  }

  return _this
}

SocketClient.prototype = events.EventEmitter.prototype
module.exports = SocketClient
