const net = require('net')
const fs = require('fs')
const path = require('path')
const events = require('events')
const os = require('os')
const core = require('node-core')
const uriUtils = require('./uri-utils')

const _ = core._

const FtpClient = function (opts) {
  const _this = this
  const _opts = opts

  let _client
  let _onConnectCB
  let _onDataSocketReadyCB
  let _onActionErrorCB
  let _onActionReadyCB
  let _onActionCompleteCB

  let dataSocketInterval
  let dataServerInterval

  const _codeHandlers = {
    125: function connectionOpenTransferStarting () {
      _onActionReadyCB.apply(_onActionReadyCB, arguments)
    },
    150: function fileStatusOkay () {
      _onActionReadyCB.apply(_onActionReadyCB, arguments)
    },
    200: function commandOkay () {
      _onActionCompleteCB.apply(_onActionCompleteCB, arguments)
    },
    202: function superflousCommand () {
      _onActionCompleteCB.apply(_onActionCompleteCB, arguments)
    },
    220: function serviceReadyForUser (message) {
      _client.send('USER', _opts.username || 'anonymous')
    },
    226: function closingDataConnectionAfterSuccessfulTransfer () {
      _onActionCompleteCB.apply(_onActionCompleteCB, arguments)
    },
    227: function enteringPassiveMode () {
      _onDataSocketReadyCB.apply(_onDataSocketReadyCB, arguments)
    },
    230: function authenticated () {
      _onConnectCB(null, true)
    },
    250: function requestFileActionOkay () {
      _onActionCompleteCB.apply(_onActionCompleteCB)
    },
    331: function usernameOkay () {
      _client.send('PASS', _opts.password || '')
    },
    332: function accountNeededForLogin () {
      _onActionErrorCB.apply(_onActionErrorCB, arguments)
    },
    425: function cantOpenConnection () {
      _onActionErrorCB.apply(_onActionErrorCB, arguments)
    },
    431: function noSuchDirectory () {
      _onActionErrorCB.apply(_onActionErrorCB, arguments)
    },
    450: function fileActionNotTaken () {
      _onActionErrorCB.apply(_onActionErrorCB, arguments)
    },
    500: function commandError () {
      _onActionErrorCB.apply(_onActionErrorCB, arguments)
    },
    503: function commandMustBePrecededByAnother () {
      _onActionErrorCB.apply(_onActionErrorCB, arguments)
    },
    530: function authenticationFailed () {
      _this.end()
      _onConnectCB('Password incorrect', false)
    },
    550: function commandError () {
      _onActionErrorCB.apply(_onActionErrorCB, arguments)
    },
    551: function commandError () {
      _onActionErrorCB.apply(_onActionErrorCB, arguments)
    },
    552: function commandError () {
      _onActionErrorCB.apply(_onActionErrorCB, arguments)
    },
    553: function commandError () {
      _onActionErrorCB.apply(_onActionErrorCB, arguments)
    }
  }

  const _createPassiveDataConnection = cb => {
    _onDataSocketReadyCB = message => {
      const ipConfig = message.substr(message.indexOf('(') + 1, message.indexOf(')') - message.indexOf('(') - 1)
      const uri = new uriUtils.URI('tcp', ipConfig)
      const dataSocket = net.connect(uri, function onConnects () {
        dataSocketInterval = setInterval(() => {
          console.log('Passive dataSocket still open')
        }, 2000)

        dataSocket.on('error', () => {
          // console.log('datasocket error')
          // console.log(arguments);
          _onActionErrorCB.apply(_onActionErrorCB, arguments)
        })
        .on('close', () => {
          clearInterval(dataSocketInterval)
        })

        return cb(null, dataSocket)
      })
    }

    _onActionErrorCB = cb
    _client.send('PASV')
  }

  const _createActiveDataConnection = cb => {
    const dataServer = net.createServer()
    let dataSocket

    dataServer.on('connection', _dataSocket => {
      dataSocket = _dataSocket
      dataSocketInterval = setInterval(() => {
        console.log('Active dataSocket still open')
      }, 2000)

      dataSocket.on('error', _onActionErrorCB)
      .on('close', function () {
        clearInterval(dataSocketInterval)
      })

      dataServer.close()
    })
    .on('error', _onActionErrorCB)
    .on('close', () => {
      // console.log('DataServer onClose')
    })

    const interfaces = os.networkInterfaces()
    const keys = _.keys(interfaces)
    let publicIP

    for (let i = 0; i < keys.length && !publicIP; i++) {
      const key = keys[i]

      for (let j = 0; i < interfaces[key].length && !publicIP; j++) {
        const inf = interfaces[key][j]

        if (inf.family === 'IPv4' && !inf.internal) {
          publicIP = inf.address
        }
      }
    }

    dataServer.listen(undefined, publicIP, function onListening () {
      dataServerInterval = setInterval(() => {
        console.log('Active dataServer still open')
      }, 2000)

      dataServer.on('close', () => {
        clearInterval(dataServerInterval)
      })

      _onActionCompleteCB = () => {
        if (dataSocket) {
          return cb(null, dataSocket)
        }

        setTimeout(_onActionCompleteCB, 100)
      }

      _onActionErrorCB = cb
      _client.send('PORT', new uriUtils.URI('tcp', dataServer.address()).to256())
    })
  }

  const _createDataConnection = cb => {
    if (_this.usePassive || false) {
      return _createPassiveDataConnection(cb)
    }

    _createActiveDataConnection(cb)
  }

  const _changePath = (pathParts, cb) => {
    if (!pathParts || !pathParts.length) {
      return cb()
    }

    _onActionErrorCB = cb
    _onActionCompleteCB = () => {
      _changePath(pathParts, cb)
    }

    _client.send('CWD', pathParts.shift())
  }

  _this.usePassive = _opts.passive || false

  _this.connect = cb => {
    let previouslyConnected = false
    let interval
    let timeout

    _onConnectCB = (err, authed) => {
      clearInterval(interval)
      cb = cb || (() => {})

      if (err) {
        return cb(err, authed)
      }

      if (!_opts.path) {
        return cb(null, authed)
      }

      const pathParts = _opts.path.split('/')
      _changePath(pathParts, err => {
        if (err) {
          return cb(err)
        }

        return cb(null, authed)
      })
    }

    _client = net.connect({ port: _opts.port || 21, host: _opts.host || 'localhost' }, function onConnect () {
      interval = setInterval(() => {
        console.log('Command Socket still open')
      }, 2000)
      clearTimeout(timeout)
    })

    _client.send = (command, message) => {
      if (message) {
        _client.write(command + ' ' + message + '\r\n')
      } else {
        _client.write(command + '\r\n')
      }
    }

    const _handleCommand = msg => {
      if (!/^[1-5][0-9][0-9]\s[\w\s]+/.test(msg)) {
        return
      }

      const parts = msg.split(' ')
      const code = +parts.shift()
      const message = parts.join(' ')

      if (code in _codeHandlers) {
        return _codeHandlers[code](message)
      }

      console.log('Unhandled event: ' + code + ' - ' + message)
      _onActionErrorCB('Unhandled event: ' + code + ' - ' + message)
    }

    _client.on('data', msg => {
      const commands = msg.toString().split('\r\n')
      for (let i = 0; i < commands.length; i++) {
        _handleCommand(commands[i])
      }
    })
    .on('error', function () {
      console.log(arguments)
      if (!previouslyConnected) {
        return cb(new Error('Could not connect'))
      }
      _this.connect(Object.__noop)
    })
    .on('end', () => {
      // console.log('CommandSocket end')
    })
    .on('close', () => {
      // console.log('CommandSocket close')
    })

    timeout = setTimeout(() => {
      _this.close()
      cb(new Error('Connect timeout expired'))
    }, _opts.timeout || 30000)
  }

  _this.uploadFile = function () {
    // TODO - this function is just going to upload file to root directory as this revision wont be including file/folder listing etc

    let filePath
    let uploadAsFilename
    let cb

    switch (arguments.length) {
      case 1:
      case 2:
        filePath = arguments[0]

        const parts = arguments[0].split(path.sep)
        uploadAsFilename = parts[parts.length - 1]
        cb = arguments[1] || (() => {})
        break
      case 3:
        filePath = arguments[0]
        uploadAsFilename = arguments[1]
        cb = arguments[2] || (() => {})
        break
      default:
        throw new Error('Argument length incompatible: FtpClient.uploadFile()')
    }

    if (typeof cb !== 'function') {
      throw new Error('Callback supplied to FtpClient.uploadFile() is not a function')
    }

    if (!_client) {
      return cb(new Error('Not connected to server. Execute connect() first'))
    }

    fs.stat(filePath, (err, stat) => {
      if (err) {
        return cb(new Error('File not found'))
      }

      _onActionErrorCB = cb

      _createDataConnection((err, dataSocket) => {
        if (err) {
          console.log('Error received but not catered for')
          console.error(err)
        }

        _onActionCompleteCB = () => {
          dataSocket.end()
          cb()
        }

        _onActionErrorCB = function () {
          if (dataSocket) {
            dataSocket.end()
          }
          cb.apply(cb, arguments)
        }

        _onActionReadyCB = () => {
          const stream = fs.createReadStream(filePath)
          stream.pipe(dataSocket)
        }

        _client.send('STOR', uploadAsFilename)
      })
    })
  }

  _this.close = _this.disconnect = _this.end = _this.destroy = () => {
    _client.destroy()
  }

  return _this
}

FtpClient.prototype = events.EventEmitter.prototype
module.exports = FtpClient
