const core = require('node-core')
const ip = require('ip')
const os = require('os')

module.exports = {
  fromIPMask: mask => {
    if (!mask) {
      return
    }

    if (Array.isArray(mask)) {
      for (let i = 0, result; i < mask.length; i++) {
        result = module.exports.fromIPMask(mask[i])
        if (result) {
          return result
        }
      }
      return
    }

    const rex = new RegExp(mask)
    const ips = os.networkInterfaces()
    for (let i in ips) {
      if (ips[i] && typeof ips[i] === 'object' && ips[i].length > 0) {
        for (var j = 0; j < ips[i].length; j++) {
          if (rex.test(ips[i][j].address)) {
            return ips[i][j].address
          }
        }
      }
    }
  },
  getAddressesInRange: (startAddress, endAddress) => {
    startAddress = startAddress || process.network.firstAddress
    endAddress = endAddress || process.network.lastAddress

    if (typeof startAddress !== 'string') {
      throw new Error('addressUtils.getAddressesInRange(startAddress, endAddress) requires a string type value (or null) for startAddress')
    }
    if (typeof endAddress !== 'string') {
      throw new Error('addressUtils.getAddressesInRange(startAddress, endAddress) requires a string type value (or null) for endAddress')
    }

    const result = []
    const parts = {
      firstAddress: startAddress.split('.'),
      lastAddress: endAddress.split('.')
    }
    const ranges = {}

    const getRangeInPart = (first, last) => {
      const result = []

      for (let i = first; i <= last; i++) {
        result.push(i)
      }

      return result
    }

    for (let i = 0; i < 4; i++) {
      ranges[i] = getRangeInPart(parts.firstAddress[i], parts.lastAddress[i])
    }

    for (let one = 0; one < ranges['0'].length; one++) {
      for (let two = 0; two < ranges['1'].length; two++) {
        for (let three = 0; three < ranges['2'].length; three++) {
          for (let four = 0; four < ranges['3'].length; four++) {
            result.push([ranges['0'][one], ranges['1'][two], ranges['2'][three], ranges['3'][four]].join('.'))
          }
        }
      }
    }

    return result
  }
}

let _ip = ip.address()
if (core.config.preferredIPMask) {
  _ip = module.exports.fromIPMask(core.config.preferredIPMask)
}

process.network = ip.subnet(_ip, ip.fromPrefixLen(core.config.subnetMaskLength || 24))
process.network.myAddress = _ip
